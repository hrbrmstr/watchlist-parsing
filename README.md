watchlist-parsing
=================

**DEPRECATED**: Merged into [AnalysisTools](/vz-risk/AnalysisTools)

Parse MSS watchlist data. Basic process requirements:

1. Unzip and parse each file in an input set
2. Combine all uncompressed files into one data set
3. Sort by:
  * IP address
  * Sources (dump into one field)
  * Combined count (across all files in the input set)

Each input file may contain duplicates. That is, the same IP address will come from a different watchlist but with the same count. Input sets to be received weekly. That is to say, one count per address per region (discard repeat counts but preserve the watchlist names), then sum with other regions.
